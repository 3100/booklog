# -*- coding: utf-8 -*-
#/usr/bin/ruby

require 'net/http'
require 'json'

class BookLog
  HOST = "api.booklog.jp"
  def initialize
  end

  # ebookなどASINのないものは非対応
  def BookLog.get_asin_list(user, status=nil, count=10)
    json = get_json(user, status, count)
    unless json then return nil end
    json["books"].select{|x|
      # xはArrayも有り得る
      x.instance_of?(Hash) && x.has_key?("asin")
    }.map do |book|
      book["asin"]
    end
  end

  def BookLog.get_json(user, status, count)
    url = get_url(user, status, count)
    warn url
    begin
      res = Net::HTTP.get(HOST, url)
      json = JSON[res]
    rescue => exception
      warn exception
      nil
    end
  end

  def BookLog.get_url(user, status, count)
    url = "/json/#{user}\?count=#{count}"
      if status
        url += "\&status=#{status}"
      end
    url
  end
end

if __FILE__ == $0
def main
  if ARGV.count < 3
    warn <<EOF
ruby book_log.rb user status count

status
  0: all
  1: wish
  2: reading
  3: done
  4: zzz..
EOF
    exit(1)
  end

  p BookLog.get_asin_list(*ARGV)
end

main
end



